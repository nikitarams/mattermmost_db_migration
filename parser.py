import re
import itertools

file=open("mmost.sql",'r')
t = file.read()
t = re.sub(r";(?!$)","", t)
rd  = {'"Id", CreateAt, UserId, "Action", ExtraInfo, IpAddress, SessionId':
'id, createat, userid, action, extrainfo, ipaddress, sessionid',
'ChannelId, UserId, Roles, LastViewedAt, MsgCount, MentionCount, NotifyProps, LastUpdateAt':
'channelid, userid, roles, lastviewedat, msgcount, mentioncount, notifyprops, lastupdateat',
'"Id", CreateAt, UpdateAt, EditAt, DeleteAt, IsPinned, UserId, ChannelId, RootId, ParentId, OriginalId, Message, "Type", Props, Hashtags, Filenames, FileIds, HasReactions':
'id, createat, updateat, editat, deleteat, ispinned, userid, channelid, rootid, parentid, originalid, message, type, props, hashtags, filenames, fileids, hasreactions',
'"Id", CreateAt, UpdateAt, DeleteAt, TeamId, "Type", DisplayName, "Name", "Header", Purpose, LastPostAt, TotalMsgCount, ExtraUpdateAt, CreatorId':
'id, createat, updateat, deleteat, teamid, type, displayname, name, header, purpose, lastpostat, totalmsgcount, extraupdateat, creatorid',
'"Token", CreateAt, "Type", Extra':
'token, createat, type, extra',
'UserId, Category, "Name", "Value"':
'userid, category, name, value',
'UserId, Status, Manual, LastActivityAt':
'userid, status, manual, lastactivityat',
'TeamId, UserId, Roles, DeleteAt':
'teamid, userid, roles, deleteat',
'"Id", CreateAt, UpdateAt, DeleteAt, Username, "Password", AuthData, AuthService, Email, EmailVerified, Nickname, FirstName, LastName, "Position", Roles, AllowMarketing, Props, NotifyProps, LastPasswordUpdate, LastPictureUpdate, FailedAttempts, Locale, MfaActive, MfaSecret':
'id, createat, updateat, deleteat, username, password, authdata, authservice, email, emailverified, nickname, firstname, lastname, position, roles, allowmarketing, props, notifyprops, lastpasswordupdate, lastpictureupdate, failedattempts, locale, mfaactive, mfasecret',
'"Id", CreateAt, UpdateAt, DeleteAt, DisplayName, "Name", Description, Email, "Type", CompanyName, AllowedDomains, InviteId, AllowOpenInvite':
'id, createat, updateat, deleteat, displayname, name, description, email, type, companyname, alloweddomains, inviteid, allowopeninvite',
'"Id", "Token", CreateAt, ExpiresAt, LastActivityAt, UserId, DeviceId, Roles, IsOAuth, Props':
'id, token, createat, expiresat, lastactivityat, userid, deviceid, roles, isoauth, props'}
cachedict = {}

def strrep(orig, repdict):
    for k,v in repdict.items():
        if k in cachedict:
            pattern = cachedict[k]
        else:
            pattern = re.compile(k, re.IGNORECASE)
            cachedict[k] = pattern
        orig = pattern.sub(v, orig)
    return orig

tt = strrep(t, rd)

tt = tt.splitlines()


        

out_file = open("mmost_out_psql.sql","w")
out_file.write(strrep(tt, rd))